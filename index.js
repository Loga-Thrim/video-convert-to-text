const express = require("express");
const ffmpeg = require("fluent-ffmpeg");
const fileUpload = require("express-fileupload");
const uniqueStringGen = require("unique-string-generator");
const path = require("path");
const { Storage } = require("@google-cloud/storage");
const speech = require('@google-cloud/speech');
const mongoClient = require("mongodb").MongoClient;
const ObjectId = require('mongodb').ObjectId; 
const fs = require("fs");

const port = process.env.PORT || 80;
const mongoUrl = process.env.MONGO_URL || "mongodb://mongo:27017";
const ffmpegPath = process.env.FFMPEG_PATH || __dirname + "/ffmpeg/bin/ffmpeg.exe";
const keyFilename = process.env.keyFilename || path.join(__dirname + "/top-news-6b7c7-b79fa5d8d8b6.json")

ffmpeg.setFfmpegPath("/usr/bin/ffmpeg");
ffmpeg.setFfprobePath("/usr/bin/ffprobe");
/* ffmpeg.setFfmpegPath(ffmpegPath); */

const gc = new Storage({ 
  keyFilename,
  projectId: 'top-news-6b7c7'
})
const filesBucket = gc.bucket('convert-speechtotext');

async function toText(path){
  return new Promise(async (resolve, reject)=>{
    try{
      const client = new speech.SpeechClient();

      const audio = {
        uri: path
      };
      const config = {
        encoding: 'MP3',
        sampleRateHertz: 8000,
        enable_automatic_punctuation: true,
        enable_word_time_offsets: true,
        languageCode: 'th-TH',
      };
      const request = {
        audio: audio,
        config: config,
      };
      
      const [operation] = await client.longRunningRecognize(request);
      const [response] = await operation.promise();
      const transcription = response.results
        .map(result => result.alternatives[0].transcript)
        .join('\n');
      resolve(transcription)
    } catch(e){
      console.log("Error "+ e);
    }
  })
}

const app = express();
app
    .use(fileUpload({
        useTempFiles: true,
        tempFileDir: "/video/"
    }))
    .get("/", (req, res)=> res.sendFile(__dirname + "/views/index.html"))
    .get("/transcript", (req, res)=>res.sendFile(__dirname + "/views/transcript.html"))
    .get("/history", (req, res)=>res.sendFile(__dirname + "/views/history.html"))

    .get("/get-transcript/:id", (req, res)=>{
      const id = req.params.id;
      mongoClient.connect(mongoUrl, {useUnifiedTopology: true},(err, db)=>{
        if(err) throw err;
        const dbcon = db.db("videototext");
        dbcon.collection("generated").findOne({_id: new ObjectId(id)}, (err, doc)=>{
          if(err) throw err;
          res.json({transcript: doc.transcript});
        })
      })
    })
    .get("/get-history", (req, res)=>{
      mongoClient.connect(mongoUrl, {useUnifiedTopology: true},(err, db)=>{
        if(err) throw err;
        const dbcon = db.db("videototext");
        dbcon.collection("generated").find({}).toArray((err, result)=>{
          res.send({transcripts: result});
        })
      })
    }) 
    .post("/convert", (req, res)=>{
      const { videoName } = req.body;
      const uniqueString = uniqueStringGen.UniqueString();
      req.files.mp4.mv("video/" + req.files.mp4.name, err=>{
        if(err) throw err;
        mongoClient.connect(mongoUrl, { useUnifiedTopology: true },(err, db)=>{
          const dbcon = db.db("videototext");
          const time = new Date().toUTCString();
          const insertObj = {
            name: videoName,
            time,
            transcript: ""
          }
          dbcon.collection("generated").insert(insertObj, (err, docs)=>{
            if(err) throw err;
            const id = docs.ops[0]._id;
            res.json({status: 'success', id});

            ffmpeg("video/" + req.files.mp4.name).withOutputFormat("mp3")
            .saveToFile(__dirname + "/audio/" + uniqueString + ".mp3")
            .on("end", ()=>{
              fs.unlinkSync("video/" + req.files.mp4.name);
              filesBucket.upload(__dirname + "/audio/" + uniqueString + ".mp3", (err, file)=>{
                if(err) throw err;
                fs.unlinkSync(__dirname + "/audio/" + uniqueString + ".mp3");
                const videoPath = `gs://convert-speechtotext/${file.name}`;
                toText(videoPath).then(transcript=>{
                  if(err) throw err;
                  dbcon.collection("generated").update({_id: ObjectId(id)}, {$set: {transcript}})
                })
              })
            })
          })
        })
      })
    })
    .get("/verifyauth", (req, res)=>{
      const storage = new Storage();
      async function listBuckets() {
        try {
          const results = await storage.getBuckets();

          const [buckets] = results;

          console.log('Buckets:');
          buckets.forEach(bucket => {
            console.log(bucket.name);
          });
        } catch (err) {
          console.error('ERROR:', err);
        }
      }
      listBuckets();
    })
    .listen(port, ()=>console.log(`> App on port ${port}.`));